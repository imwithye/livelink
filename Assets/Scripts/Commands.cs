﻿using FileStream;
using UnityEngine;
using Material = UnityEngine.Material;

public class Commands : MonoBehaviour
{
    public string directoryPath;
    public Material defaultMaterial;
    public GameObject rendererRoot;
    private Builder _builder;

    private void Awake()
    {
        directoryPath = Options.Parse().FolderName == null ? directoryPath : Options.Parse().FolderName;
        _builder = new Builder(new BuilderOption {DefaultMaterial = defaultMaterial, RendererRoot = rendererRoot},
            directoryPath);
        Debug.Log($"Opened resources directory: {directoryPath}");
    }

    private void OnDestroy()
    {
        _builder.Dispose();
    }
}