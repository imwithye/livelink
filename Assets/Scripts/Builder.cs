using System;
using System.Threading.Tasks;
using FileStream;
using UnityEngine;

public class BuilderOption
{
    public UnityEngine.Material DefaultMaterial;
    public GameObject RendererRoot;
}

public class Builder : IDisposable
{
    public BuilderOption Option { get; }
    public Transport Transport { get; }
    public Task BuildTask { get; private set; }

    private readonly Renderers _renderers;
    private readonly object _changeLock = new object();
    private readonly object _buildTaskLock = new object();

    public Builder(BuilderOption option, string directoryPath)
    {
        Option = option;
        Transport = new PipeTransport(directoryPath, OnReceivedBatchJob);
        Transport.StartClient();
        _renderers = new Renderers(this);
    }

    private void OnReceivedBatchJob(object sender, Batch batch)
    {
        switch (batch.Action)
        {
            case BatchAction.AddTriangleMeshInstance:
                Debug.Log("AddTriangleMeshInstance");
                Transport.TryGet(Guid.Empty, out TriangleMeshInstance _, false, true);
                foreach (var id in batch.Ids)
                {
                    if(!Transport.TryGet(id, out TriangleMeshInstance inst)) continue;
                    _renderers.TryAdd(inst);
                }

                TryBuild(out _);
                break;
            case BatchAction.UpdateTriangleMeshInstance:
                foreach (var id in batch.Ids)
                {
                    if(!Transport.TryGet(id, out TriangleMeshInstance inst)) continue;
                    _renderers.TryDelete(inst);
                }
                Transport.TryGet(Guid.Empty, out TriangleMeshInstance _, false, true);
                foreach (var id in batch.Ids)
                {
                    if(!Transport.TryGet(id, out TriangleMeshInstance inst)) continue;
                    _renderers.TryAdd(inst);
                }
                break;
            case BatchAction.DeleteTriangleMeshInstance:
                Debug.Log("DeleteTriangleMeshInstance");
                foreach (var id in batch.Ids)
                {
                    if(!Transport.TryGet(id, out TriangleMeshInstance inst)) continue;
                    _renderers.TryDelete(inst);
                }
                
                TryBuild(out _);
                break;
            case BatchAction.UpdateMaterial:
                Debug.Log("UpdateMaterial");
                break;
            default:
                Debug.LogWarning("Unknown batch action");
                break;
        }
    }

    public bool TryBuild(out Task buildTask)
    {
        lock (_buildTaskLock)
        {
            if (BuildTask == null || BuildTask.IsCompleted || BuildTask.IsCanceled || BuildTask.IsFaulted)
            {
                lock (_changeLock)
                {
                    BuildTask = _renderers.Build();
                    buildTask = BuildTask;
                    return true;
                }
            }

            buildTask = null;
            return false;
        }
    }

    public void Dispose()
    {
        Transport?.Dispose();
    }
}