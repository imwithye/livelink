﻿using UnityEngine;

public class DebugCamera : MonoBehaviour
{
    public float speedXY = 0.5f;
    public float speedXZ = 3f;
    public float speedForward = 5f;
    public float angleSpeed = 3f;

    private Vector3 _preMousePosition;
    private Vector3 _targetEuler;
    private Vector3 _targetPos;

    void Start()
    {
        _preMousePosition = Input.mousePosition;
        var thisTransform = transform;
        _targetPos = thisTransform.position;
        _targetEuler = thisTransform.rotation.eulerAngles;
    }

    void Update()
    {
        UpdateXY();
        UpdateXYZ();
        UpdateForward();
        UpdateRotation();
        UpdateMousePosition();
        UpdateTransform();
    }

    void UpdateXY()
    {
        if (Input.GetMouseButton(2) && !Input.GetMouseButtonDown(2))
        {
            var dPos = Input.mousePosition - _preMousePosition;
            var velocity = ((-transform.up) * dPos.y + (-transform.right) * dPos.x) * speedXY;
            _targetPos += velocity * Time.deltaTime;
        }
    }

    void UpdateXYZ()
    {
        var x = Input.GetKey(KeyCode.A) ? -1 : (Input.GetKey(KeyCode.D) ? 1 : 0);
        var y = Input.GetKey(KeyCode.Q) ? -1 : (Input.GetKey(KeyCode.E) ? 1 : 0);
        var z = Input.GetKey(KeyCode.S) ? -1 : (Input.GetKey(KeyCode.W) ? 1 : 0);

        var velocity = Quaternion.AngleAxis(_targetEuler.y, Vector3.up) * (new Vector3(x, y, z)) * speedXZ;
        _targetPos += velocity * Time.deltaTime;
    }

    void UpdateForward()
    {
        var x = Input.mouseScrollDelta.x;
        var z = Input.mouseScrollDelta.y;

        var velocity = (transform.forward * z + transform.right * x) * speedForward;
        _targetPos += velocity * Time.deltaTime;
    }

    void UpdateRotation()
    {
        if ((Input.GetMouseButton(0) && !Input.GetMouseButtonDown(0)) ||
            (Input.GetMouseButton(1) && !Input.GetMouseButtonDown(1)))
        {
            var dPos = Input.mousePosition - _preMousePosition;
            _targetEuler += new Vector3(-dPos.y, dPos.x, 0f) * (Time.deltaTime * angleSpeed);
            _targetEuler.x = Mathf.Clamp(_targetEuler.x, -90f, 90f);
        }
    }

    void UpdateMousePosition()
    {
        _preMousePosition = Input.mousePosition;
    }

    void UpdateTransform()
    {
        transform.position = Vector3.Lerp(transform.position, _targetPos, 0.5f);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(_targetEuler), 0.5f);
    }
}
