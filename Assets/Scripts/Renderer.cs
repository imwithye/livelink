﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Rendering;
using FileStream;
using Object = UnityEngine.Object;

public class Renderer
{
    public const int MaxVerticesCount = 65535;
    public readonly Guid MaterialId;
    public GameObject RendererObject { get; private set; }
    private readonly ConcurrentValue<int> _verticesCount;
    private readonly ConcurrentValue<bool> _needsRebuild;
    private readonly Builder _builder;
    private readonly Renderers _renderers;
    private readonly ConcurrentDictionary<Guid, TriangleMeshInstance> _triangleMeshInstances;

    public Renderer(Builder builder, Renderers renderers, Guid materialId)
    {
        MaterialId = materialId;
        _verticesCount = new ConcurrentValue<int>(0);
        _needsRebuild = new ConcurrentValue<bool>(false);
        _builder = builder;
        _renderers = renderers;
        _triangleMeshInstances = new ConcurrentDictionary<Guid, TriangleMeshInstance>();
    }

    public bool TryAppend(TriangleMeshInstance instance)
    {
        if (_triangleMeshInstances.ContainsKey(instance.Id))
        {
            return true;
        }

        if (!_builder.Transport.TryGet(instance.MeshId, out TriangleMeshEntry meshEntry))
        {
            return false;
        }

        if (_verticesCount.Value == 0 && meshEntry.NVertices > MaxVerticesCount)
        {
            _triangleMeshInstances[instance.Id] = instance;
            _verticesCount.Value += meshEntry.NVertices;
            _needsRebuild.Value = true;
            return true;
        }

        if (meshEntry.NVertices + _verticesCount.Value > MaxVerticesCount) return false;

        _triangleMeshInstances[instance.Id] = instance;
        _verticesCount.Value += meshEntry.NVertices;
        _needsRebuild.Value = true;
        return true;
    }

    public bool TryDelete(TriangleMeshInstance instance)
    {
        if (!_triangleMeshInstances.ContainsKey(instance.Id))
        {
            return false;
        }

        if (!_builder.Transport.TryGet(instance.MeshId, out TriangleMeshEntry meshEntry))
        {
            return false;
        }

        if (!_triangleMeshInstances.TryRemove(instance.Id, out _))
        {
            return false;
        }

        _verticesCount.Value -= meshEntry.NVertices;
        _needsRebuild.Value = true;
        return true;
    }

    public Task Build()
    {
        if (!_needsRebuild.Value)
        {
            return Task.CompletedTask;
        }

        return Task.Factory.StartNew(() =>
        {
            var vertices = new List<UnityEngine.Vector3>();
            var triangles = new List<int>();
            foreach (var triangleMeshInstance in _triangleMeshInstances.Values)
            {
                if (!_builder.Transport.TryGet(triangleMeshInstance.MeshId, out TriangleMesh mesh))
                {
                    continue;
                }

                var count = vertices.Count;
                vertices.AddRange(mesh.Vertices.Select(v => v.ToWorldPos(triangleMeshInstance.Transform))
                    .Select(v => new UnityEngine.Vector3(v.X, v.Y, v.Z)));
                triangles.AddRange(mesh.Indices.Select(t => t + count));
            }

            return (vertices, triangles);
        }).ContinueWith((t) =>
        {
            if (t.Result.vertices.Count == 0 || t.Result.triangles.Count == 0)
            {
                Object.DestroyImmediate(RendererObject);
                RendererObject = null;
                return;
            }

            if (RendererObject == null)
            {
                RendererObject = new GameObject(MaterialId.ToString());
                if (_builder.Option.RendererRoot != null)
                {
                    RendererObject.transform.parent = _builder.Option.RendererRoot.transform;
                }

                RendererObject.AddComponent<MeshFilter>();
                _builder.Transport.TryGet(MaterialId, out FileStream.Material materialIn);
                _renderers.ConvertMaterial(materialIn, out var materialOut);
                RendererObject.AddComponent<MeshRenderer>().sharedMaterial = materialOut;
            }

            var unityMesh = new Mesh
            {
                indexFormat = t.Result.vertices.Count > MaxVerticesCount ? IndexFormat.UInt32 : IndexFormat.UInt16,
                vertices = t.Result.vertices.ToArray(),
                triangles = t.Result.triangles.ToArray()
            };
            unityMesh.RecalculateNormals();
            RendererObject.GetComponent<MeshFilter>().sharedMesh = unityMesh;
        }, Dispatcher.MainQueueTaskScheduler()).ContinueWith(t => { _needsRebuild.Value = false; });
    }
}

public class Renderers
{
    private readonly Builder _builder;
    private readonly ConcurrentDictionary<Guid, List<Renderer>> _renderers;

    public Renderers(Builder builder)
    {
        _builder = builder;
        _renderers = new ConcurrentDictionary<Guid, List<Renderer>>();
    }

    public bool ConvertMaterial(FileStream.Material materialIn, out UnityEngine.Material materialOut)
    {
        materialOut = _builder.Option.DefaultMaterial;
        return true;
    }

    public bool TryAdd(TriangleMeshInstance instance)
    {
        if (!_renderers.ContainsKey(instance.MaterialId))
        {
            _renderers[instance.MaterialId] = new List<Renderer>();
        }

        if (_renderers[instance.MaterialId].Any(renderer => renderer.TryAppend(instance)))
        {
            return true;
        }

        lock (_renderers[instance.MaterialId])
        {
            var render = new Renderer(_builder, this, instance.MaterialId);
            _renderers[instance.MaterialId].Add(render);
            return render.TryAppend(instance);
        }
    }

    public bool TryDelete(TriangleMeshInstance instance)
    {
        return _renderers.ContainsKey(instance.MaterialId) &&
               _renderers[instance.MaterialId].Any(renderer => renderer.TryDelete(instance));
    }

    public Task Build()
    {
        var tasks = _renderers.Values.SelectMany(renderers => renderers).Select(renderer => renderer.Build())
            .ToList();
        return Task.WhenAll(tasks);
    }
}