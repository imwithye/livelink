﻿using System;
using System.IO;

public class Options
{
    private static Options _instance;
    public string FolderName;

    private static string FixString(string value)
    {
        if (value.StartsWith("\"") && value.EndsWith("\"")) value = value.Substring(1, value.Length - 2);
        else if (value.StartsWith("'") && value.EndsWith("'")) value = value.Substring(1, value.Length - 2);
        return value;
    }

    public static Options Parse(string[] args = null)
    {
        if (_instance != null) return _instance;
        return _instance = ParseNew(args);
    }

    public static Options ParseNew(string[] args = null)
    {
        args = args ?? Environment.GetCommandLineArgs();

        var options = new Options();
        foreach (var arg in args)
        {
            if (!arg.ToLower().StartsWith("--vrcollab")) continue;
            var argSplit = arg.Split('=');
            var val = argSplit.Length <= 1 ? "" : arg.Substring(argSplit[0].Length + 1);
            if (argSplit[0].ToLower() != "--vrcollab-folder-name") continue;
            options.FolderName = FixString(val);
            options.FolderName = Path.GetFullPath(options.FolderName);
        }
        return options;
    }
}