﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class MainQueueTaskScheduler : TaskScheduler
{
    private readonly ConcurrentQueue<Task> _tasks = new ConcurrentQueue<Task>();

    public void Execute()
    {
        if (!_tasks.TryDequeue(out var task))
        {
            return;
        }

        TryExecuteTask(task);
    }

    public int Count()
    {
        return _tasks.Count;
    }

    protected override IEnumerable<Task> GetScheduledTasks()
    {
        return _tasks;
    }

    protected override void QueueTask(Task task)
    {
        _tasks.Enqueue(task);
    }

    protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
    {
        return false;
    }
}

public class Dispatcher : MonoBehaviour
{
    public int lastFrameTaskCount;
    public int taskCount;
    private const long TicksYieldSlow = 200000L;
    private long _nextSlow = long.MinValue;
    private static Dispatcher _instance;
    private readonly MainQueueTaskScheduler _mainQueueTaskScheduler = new MainQueueTaskScheduler();

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Update()
    {
        lastFrameTaskCount = 0;
        _nextSlow = DateTime.UtcNow.Ticks + TicksYieldSlow;
        while (!IsYieldSlow() && _mainQueueTaskScheduler.Count() > 0)
        {
            _mainQueueTaskScheduler.Execute();
            lastFrameTaskCount++;
        }

        taskCount = _mainQueueTaskScheduler.Count();
    }

    private static bool IsYieldSlow()
    {
        return DateTime.UtcNow.Ticks > _instance._nextSlow;
    }

    public static MainQueueTaskScheduler MainQueueTaskScheduler()
    {
        return _instance._mainQueueTaskScheduler;
    }
}